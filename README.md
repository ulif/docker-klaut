# docker-klaut

Quick! Get me a nextcloud! I got some certificate already!

This stuff is based on examples from

  https://github.com/nextcloud/docker

It configures a proxy nginx running as frontend, whose config is mainly done in
`nginx/`.

Please note, that this nextcloud instance

- listens on port 8443 of the host
- requires a valid certicate and key in `nginx/`.
- (of course you also need `docker` and `docker-compose` installed)


## Prep 1: Get a certificate and key

Get a key and certificate for the domain you want to serve. This install will
be accessible from the internet and you must connect encrypted to your
nextcloud. No certificate, no connect.

Put `fullchain.pem` and `privkey.key` into the `nginx/` folder.


## Prep 2: Edit db.env

Set passwords in `db.env`, pick usernames and tell, what domains you want to
run the instance under. IOW: what domain has the host that serves this
nextcloud?


## Ready? Go!

    # docker-compose up -d

If all went well, you should be able to connect to `https://sample.org:8443`.
Log-in with the credentials from `db.env`.

Enjoy :)
